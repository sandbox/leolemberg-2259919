api = 2
core = 7.x

includes[] = drupal-org-core.make

projects[openskiresort][type] = profile
projects[openskiresort][download][type] = git
projects[openskiresort][download][branch] = 7.x-1.x
projects[openskiresort][download][url] = http://git.drupal.org/sandbox/leolemberg/2259919.git
